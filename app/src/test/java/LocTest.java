
import org.junit.Test;
import java.util.regex.Pattern;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.mka.mka.mapand.MockLocation;
import com.mka.mka.mapand.MockGPXUtil;


public class LocTest{

    @Test
    public void testDirectionNull() {
        MockGPXUtil gpx= new MockGPXUtil();
        char expected = 'N';
        char actual = gpx.checkDirection('N');
        //
        assertEquals("Null test failed", expected, actual);
    }

    @Test
    public void testDirectionEast() {
        MockGPXUtil gpx= new MockGPXUtil();
        MockLocation loc = new MockLocation();
        loc.setLongitude(25.00);
        loc.setLatitude(68.00);
        gpx.updatePositionQueue(loc);
        MockLocation loc1 = new MockLocation();
        loc1.setLongitude(25.01);
        loc1.setLatitude(68.00);
        gpx.updatePositionQueue(loc1);
        MockLocation loc2 = new MockLocation();
        loc2.setLongitude(25.02);
        loc2.setLatitude(68.00);
        gpx.updatePositionQueue(loc2);
        char expected = 'E';
        char actual = gpx.checkDirection('N');
        assertEquals("East test failed", expected, actual);
    }

    @Test
    public void testDirectionVector() {
        MockGPXUtil gpx= new MockGPXUtil();
        MockLocation loc = new MockLocation();
        loc.setLongitude(25.0);
        loc.setLatitude(68.0);
        gpx.updatePositionQueue(loc);
        MockLocation loc1 = new MockLocation();
        loc1.setLongitude(25.1);
        loc1.setLatitude(68.1);
        gpx.updatePositionQueue(loc1);
        MockLocation loc2 = new MockLocation();
        loc2.setLongitude(25.2);
        loc2.setLatitude(68.2);
        gpx.updatePositionQueue(loc2);
        gpx.showQueue();
        Double[] new_r = new Double[]{0.0, 0.0};
        double expected = 0.001;
        new_r = gpx.getDirectionVector();
        double actual = new_r[1];
        assertEquals("East VEC test failed", expected, actual,0.00001);
    }

    @Test
    public void testDirectionWest() {
        MockGPXUtil gpx= new MockGPXUtil();
        MockLocation loc = new MockLocation();
        loc.setLongitude(25.02);
        loc.setLatitude(68.01);
        gpx.updatePositionQueue(loc);
        MockLocation loc1 = new MockLocation();
        loc1.setLongitude(25.01);
        loc1.setLatitude(68.005);
        gpx.updatePositionQueue(loc1);
        MockLocation loc2 = new MockLocation();
        loc2.setLongitude(25.00);
        loc2.setLatitude(68.00);
        gpx.updatePositionQueue(loc2);
        char expected = 'W';
        char actual = gpx.checkDirection('W');
        assertEquals("West test failed", expected, actual);
    }

    @Test
    public void testDirectionNorth() {
        MockGPXUtil gpx= new MockGPXUtil();
        MockLocation loc = new MockLocation();
        loc.setLongitude(25.00);
        loc.setLatitude(68.00);
        gpx.updatePositionQueue(loc);
        MockLocation loc1 = new MockLocation();
        loc1.setLongitude(25.00);
        loc1.setLatitude(68.01);
        gpx.updatePositionQueue(loc1);
        MockLocation loc2 = new MockLocation();
        loc2.setLongitude(25.00);
        loc2.setLatitude(68.02);
        gpx.updatePositionQueue(loc2);
        char expected = 'N';
        char actual = gpx.checkDirection('N');
        assertEquals("North test failed", expected, actual);
    }

    @Test
    public void testDirectionSouth() {
        MockGPXUtil gpx= new MockGPXUtil();
        MockLocation loc = new MockLocation();
        loc.setLongitude(25.00);
        loc.setLatitude(68.02);
        gpx.updatePositionQueue(loc);
        MockLocation loc1 = new MockLocation();
        loc1.setLongitude(25.00);
        loc1.setLatitude(68.01);
        gpx.updatePositionQueue(loc1);
        MockLocation loc2 = new MockLocation();
        loc2.setLongitude(25.00);
        loc2.setLatitude(68.00);
        gpx.updatePositionQueue(loc2);
        char expected = 'S';
        char actual = gpx.checkDirection('N');
        assertEquals("South test failed", expected, actual);
    }

}