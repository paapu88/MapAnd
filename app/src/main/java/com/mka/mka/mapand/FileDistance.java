package com.mka.mka.mapand;

/**
 * Created by mka on 29.12.2017.
 */

public class FileDistance {
    /*
    class for sorting files according distance
     */
    String fileName;
    double distance;

    public FileDistance(String fileName, double distance){
        this.fileName = fileName;
        this.distance = distance;

    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }


}
