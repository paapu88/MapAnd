package com.mka.mka.mapand;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.view.GestureDetectorCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import android.support.v4.app.ActivityCompat;
import android.support.design.widget.Snackbar;
import com.google.android.gms.location.LocationResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.Date;
import android.os.Looper;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;


// TODO position off when near edge of the map (now wrong position when rotated


public class MainActivity extends Activity implements
        GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener{

    boolean isGPS = false;
    private static final LinkedHashMap<String, Double> mapScales = new LinkedHashMap<>();
    static {
        mapScales.put("1:4000", 0.00025);
        mapScales.put("1:10000", 0.0001);
        mapScales.put("1:15000", 1.0/15000.0);
        mapScales.put("1:20000", 0.00005);
        mapScales.put("1:50000", 0.00002);
        mapScales.put("1:200000",0.000005);
    }


    // google-location begin
    // https://github.com/googlesamples/android-play-location/blob/master/LocationUpdates/app/src/main/java/com/google/android/gms/location/sample/locationupdates/MainActivity.java
    public String TAG = "GOOGLE-GPS:";
    private static final int READ_REQUEST_CODE = 42;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    // Keys for storing activity state in the Bundle.
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final static String KEY_LOCATION = "location";
    private final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    private Location mCurrentLocation; // Represents a geographical location.
    private String mLastUpdateTime; //Time when the location was updated represented as a String.
    private FusedLocationProviderClient mFusedLocationClient; //Provides access to the Fused Location Provider API.
    private SettingsClient mSettingsClient; // Provides access to the Location Settings API.
    private LocationRequest mLocationRequest; // Stores parameters for requests to the FusedLocationProviderApi.
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 2000; // The desired interval for location updates. Inexact. Updates may be more or less frequent.
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2; //The fastest rate for active location updates.
    private LocationCallback mLocationCallback; // Callback for Location events.
    private LocationSettingsRequest mLocationSettingsRequest; //Stores the types of location services the client is interested in using.
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private Boolean mRequestingLocationUpdates;
    //google location end

    Location oldLocation;
    MyDisplay myDisplay;            // metrics related to the device
    ImageView mShowMap;        // the view we use in this activity
    Tiles tiles;               // services related getting the correct map images from mapant server
    NetworkUtils networkUtils; // methods related to drawing of the map
    Bitmap map;                // the loaded full map on the background
    double mapScale;           // the scale of the map
    GPXUtil gpxUtil;           // services related to gpx tracks
    //List<FileDistance> tracks; // names of tracks,FdisplayT and distances from current position to tracks
    private Menu menu;           // a copy of menu (for manipulating it)
    private ArrayList<String> courseNames; // orienteering course names from heroku
    private int maxCount;      // the last menu index for files (before heroku gpx)
    private int maxCount2;     // the last menu index for o-courses
    String gpxFileName;
    String gpxHerokuName;
    boolean showMapOnly = false;  // if true, we load map nearby, nothing else
    char mapDirection;            // bearing  that is pointing upwards on the device (can be N, S, E, W)
    private GestureDetectorCompat mDetector;
    private boolean autoRotate;
    private boolean something_selected;   // when true we can start first download

    public MainActivity() {
        oldLocation = null;
        mShowMap = null;
        tiles = new Tiles();
        // for toasts: gpxUtil = new GPXUtil(MainActivity.this);
        gpxUtil = new GPXUtil();
        networkUtils = new NetworkUtils(this, gpxUtil);
        courseNames = null;
        maxCount = 0;
        maxCount2 = 0;
        mapScale = 0.0001;  // initially 1:10000 map scale
        gpxFileName = null;
        gpxHerokuName = null;
        mapDirection = ' ';
        this.autoRotate = true;
        this.something_selected = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Intent done = new Intent(MainActivity.this, MenuActivity.class);
        //startActivityForResult(done,1);

        this.gpxUtil.getCourseNames(this);

        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        myDisplay = new MyDisplay(display, dm, this.mapScale);
        networkUtils.setMin_tiles_x(myDisplay.getNumberOfTilesX());
        networkUtils.setMin_tiles_y(myDisplay.getNumberOfTilesY());
        networkUtils.setControlEtcSizesInPixels(myDisplay.getMilliMeterToPixel());

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);
        // location stuff
        // https://developer.android.com/training/location/change-location-settings.html
        //
        mRequestingLocationUpdates = true;

        // Update values using data stored in the Bundle.
        updateValuesFromBundle(savedInstanceState);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        // Kick off the process of building the LocationCallback, LocationRequest, and
        // LocationSettingsRequest objects.
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(MainActivity.this,
                                0x1);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
        startLocationUpdates();
        //permission end

        mShowMap = (ImageView) findViewById(R.id.iv_show_map);
        // Instantiate the gesture detector with the
        // application context and an implementation of
        // GestureDetector.OnGestureListener
        mDetector = new GestureDetectorCompat(this,this);
        // Set the gesture detector as the double tap
        // listener.
        mDetector.setOnDoubleTapListener(this);
    }


     public void showMap()  {
        /*
        draw map, point and track
         */
        // in case of missing gps signal, but existing track/o-course, set location to start
        if (mCurrentLocation==null && gpxUtil.getStartLocation()!=null){
            mCurrentLocation=gpxUtil.getStartLocation();
        }
        // no track, no gps signal, we cannot proceed here
        if (mCurrentLocation == null) {
            Toast.makeText(MainActivity.this, "Waiting for GPS signal...!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        if (!this.something_selected){
            //user has not selected anything yet related to loading a map
            return;
        }


         int[] xyz;
        xyz = tiles.degs2XYZ(mCurrentLocation.getLongitude(),
                mCurrentLocation.getLatitude(), myDisplay.getZoomLevel());
        int x = xyz[0];
        int y = xyz[1];
        float[] xyPercent = tiles.degs2Percentage(mCurrentLocation.getLongitude(),
                mCurrentLocation.getLatitude(), myDisplay.getZoomLevel());

        //double ok_move = (double) (min(myDisplay.getXInNature(),
        //        myDisplay.getYInNature())) / 2.0d * 0.9d;
        double ok_move = Math.min(myDisplay.getWholeMapInNatureX(), myDisplay.getWholeMapInNatureY())/ 2.0 * 0.9d;

        // no track or map too big: map is loaded from www when we have moved away from the center of loaded map
        if (this.showMapOnly || gpxUtil.getTooBig()){
            // Log.d("MAIN","TOO BIG1");
            if (oldLocation == null || mCurrentLocation.distanceTo(oldLocation) > ok_move) {
                if (oldLocation == null){
                    Log.d("OLD LOCATION NULL", " ");
                }else {
                    Log.d("MAIN:", "OLD:" + oldLocation + " dist " + mCurrentLocation.distanceTo(oldLocation) + " ok_move:" + ok_move);
                }
                oldLocation = mCurrentLocation;
                networkUtils.setBigBitmapNearBy(myDisplay.getNumberOfTilesX(),
                        myDisplay.getNumberOfTilesY(),
                        x, y, myDisplay.getZoomLevel());
            }
        }
        // if map-load from www is done at least once, and
        // we have not changed to 'load whole map'
        if (networkUtils.getMapLoaded()) {

            // auto rotation
            if (this.autoRotate){
                gpxUtil.updatePositionQueue(mCurrentLocation);
                this.mapDirection = gpxUtil.checkDirection(this.mapDirection);
            }
            map = networkUtils.setImg(mShowMap,
                    x, y, myDisplay.getZoomLevel(),
                    xyPercent[0], xyPercent[1],
                    (float) myDisplay.getxInPixels(),
                    (float) myDisplay.getyInPixels(),
                    this.mapDirection);
            networkUtils.setPoint(map, mShowMap,
                    (float) myDisplay.getxInPixels(), (float) myDisplay.getyInPixels(),
                    this.mapDirection);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //super.onPrepareOptionsMenu(menu);

        menu.clear();
        int count = 1;
        SubMenu sMenu = menu.addSubMenu(Menu.NONE, R.id.settings_menu_item, Menu.NONE, R.string.settings);
        if (this.autoRotate) {
            sMenu.add(0, 10 + count, 0, "Autorotate is on. Switch it off");
        }else{
            sMenu.add(0, 10 + count, 0, "Autorotate is off. Switch it on");
        }
        count ++;
        for (String scale : MainActivity.mapScales.keySet()) {
            sMenu.add(0, 10 + count, 0, scale);
            count++;
        }
        this.maxCount = count -1;
        SubMenu sMenu2 = menu.addSubMenu(Menu.NONE, R.id.course_menu_item, Menu.NONE, R.string.load_course);
        if (this.courseNames != null) {
            for (String courseName : this.courseNames) {
                sMenu2.add(0, 10 + count, 0, courseName);
                count++;
            }
        }
        this.maxCount2 = count -1;
        SubMenu sMenu3 = menu.addSubMenu(Menu.NONE, R.id.file_menu_item, Menu.NONE, R.string.load_file);
        for (String fileName: gpxUtil.getgpxFileNames()) {
            String[] fnames = fileName.split("/");
            sMenu3.add(0, 10+count, 0, fnames[fnames.length-1]);
            count++;
        }
        getMenuInflater().inflate(R.menu.activity_menu, menu);//Menu Resource, Menu
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (    id==R.id.file_menu_item||
                id==R.id.course_menu_item||
                id==R.id.show_map_only||
                id==R.id.load_tracks||
                id==R.id.settings_menu_item||
                id==R.id.help||
                id==R.id.exit) {
            // non-file load options
            switch (item.getItemId()) {
                case R.id.show_map_only:
                    // show map only
                    this.something_selected=true;
                    networkUtils.setshowTrack(false);
                    this.showMapOnly=true;
                    gpxUtil.setloadMapFromWWW(true); // can download map
                    oldLocation = null; // this causes reloading the map from internet
                    try {
                        Toast.makeText(MainActivity.this, "Loading map...!",
                                Toast.LENGTH_LONG).show();
                    showMap();
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "Problem with loading the map...!",
                                Toast.LENGTH_LONG).show();
                    }
                    return true;
                case R.id.load_tracks:
                    // load tracks from jalki.fi
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://jalki.fi"));
                    startActivity(browserIntent);
                    return true;
                case R.id.file_menu_item:
                    // load track/course from local file
                    //this.invalidateOptionsMenu();
                    this.performFileSearch();
                    this.something_selected=true;
                    return true;
                case R.id.course_menu_item:
                    // load o-course from database in heroku
                    if (this.courseNames != null) {
                        this.invalidateOptionsMenu();}
                    else{
                        // o-courses not yet loaded, return to the main menu
                        this.openOptionsMenu();
                    }
                    this.something_selected=true;
                    return true;
                case R.id.settings_menu_item:
                    this.invalidateOptionsMenu();
                    return true;
                case R.id.help:
                    Intent intent = new Intent(this, DisplayHelpActivity.class);
                    String message = "THIS APP ONLY WORKS IN FINLAND.\n\n" +
                            "You get Menu by pushing activity_menu button of your device (typically lower left). " +
                            "Or by a double tap on the screen. \n\n" +
                            "You see a map around you initially in 1:10000 scale. \n\n" +
                            "Your location is shown as a red ball. \n\n" +
                            "Options: \n\n" +
                            "- [Settings and Map scales]Set map 90deg autorotation on/off. " +
                            "Change the scale of the map. \n\n" +
                            "- [Select orienteering course] Download and run an orienteering course. "+
                            " To upload your own track or orienteering course use "+
                            " https://github.com/paapu88/mapAndDB/blob/master/gpxFile2Heroku.py \n\n"+
                            "- [Load track/course from device] Select a gpx track/orienteering course stored on your device.\n\n"+
                            "- [Load track(s) from jalki.fi] Download gps-track from jalki.fi or elsewhere " +
                                    " to your device. \n\n"+
                            "- [Show map only] Show the map around you. \n\n ";
                    intent.putExtra("help_message", message);
                    startActivity(intent);
                    return true;
                case R.id.exit:
                    // exit program
                    finish();
                    System.exit(0);
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
        else{
            // scales
            if ((item.getItemId()-11) < this.maxCount) {
                if (item.getItemId()==11){
                    this.autoRotate = !this.autoRotate;
                    this.gpxUtil.resetPositionQueue();
                    this.invalidateOptionsMenu();
                    //Log.d("MAIN"," autorotate changed");
                }else {
                    ArrayList<Double> l = new ArrayList<Double>(this.mapScales.values());
                    this.mapScale = l.get((item.getItemId() - 12));
                    WindowManager windowManager = getWindowManager();
                    Display display = windowManager.getDefaultDisplay();
                    DisplayMetrics dm = new DisplayMetrics();
                    display.getMetrics(dm);
                    myDisplay = new MyDisplay(display, dm, this.mapScale);
                    networkUtils.setMin_tiles_x(myDisplay.getNumberOfTilesX());
                    networkUtils.setMin_tiles_y(myDisplay.getNumberOfTilesY());
                    networkUtils.setControlEtcSizesInPixels(myDisplay.getMilliMeterToPixel());
                    if (this.something_selected){ // previously loaded map is reloaded
                                                    // in other scale
                        int[] xyz;
                        //Log.d("MAINNN"," "+mCurrentLocation.getLongitude());
                        xyz = tiles.degs2XYZ(mCurrentLocation.getLongitude(),
                                mCurrentLocation.getLatitude(), myDisplay.getZoomLevel());
                        int x = xyz[0];
                        int y = xyz[1];
                        networkUtils.setBigBitmapNearBy(myDisplay.getNumberOfTilesX(),
                                myDisplay.getNumberOfTilesY(),
                                x, y, myDisplay.getZoomLevel());
                    }

                }
            }else if ((item.getItemId()-11) < this.maxCount2) { // o-courses from heroku
                this.something_selected=true;
                gpxUtil.setloadMapFromWWW(false); //prevent map load when reading gpx
                this.showMapOnly = false;
                networkUtils.setCopyComboBitmapNull(); // new track, delete old copy
                gpxHerokuName = this.courseNames.get(item.getItemId() - this.maxCount-11);
                gpxUtil.readHerokuTrack(this, gpxHerokuName);

            }else{
                // tracks from local dir
                // track from a local file
                this.something_selected=true;
                gpxUtil.setloadMapFromWWW(false); //prevent map load when reading gpx
                this.showMapOnly=false;
                networkUtils.setCopyComboBitmapNull(); // new track, delete old copy
                gpxFileName = gpxUtil.getgpxFileNames().get(item.getItemId() - this.maxCount2- 11);
                try{gpxUtil.readGPXTrack(gpxFileName);}catch (Exception e){};
                this.showMapWithTrack(gpxUtil.getGPXList());
                }
        }
        //https://stackoverflow.com/questions/17311833/how-we-can-add-menu-item-dynamically
        return true;
    }

    // Get a MemoryInfo object for the device's current memory status.
    private ActivityManager.MemoryInfo getAvailableMemory() {
        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo;
    }


    void showMapWithTrack(List<Location> gpxList){
        //Log.d("MAIN:"," "+gpxList.toString());
        if (gpxList==null){return;};
        // draw map based on track (o-course or gpx track)
        gpxUtil.setMaxMinLatLon(gpxList);
        //Log.d("MAIN max lon:", " "+gpxUtil.getMaxLongitude());
        gpxUtil.setloadMapFromWWW(true);
        networkUtils.setshowTrack(true);
        //this.networkUtils.setFileNames(gpxFileName, gpxHerokuName);
        this.oldLocation = null;
        // check whether the track/course can be loaded in one shot
        ActivityManager.MemoryInfo mi = getAvailableMemory();
        long availableMegs = mi.availMem / 1048576L;
        networkUtils.calculateTileRangeforTrack(myDisplay.getZoomLevel());

        if (!gpxUtil.isTrackTooBig(networkUtils.getNumberOfTiles(), availableMegs)) {
            this.setBigMap();
        }
        this.showMap();
    }

    public void setBigMap() {
        // initialize big map when loading the whole map at one shot
        oldLocation = null; // this causes reloading the map from internet
        networkUtils.setBigBitmap(myDisplay.getZoomLevel());
        networkUtils.setMapLoaded(false);
    }
    @Override
    public void onAttachedToWindow() {
        openOptionsMenu();
    };



    public void setCourseNames(String allNames){
        // this is called from async task in GPXUtil
        this.courseNames = new ArrayList<>(Arrays.asList(allNames.split(" ")));
        Collections.sort(courseNames, Collections.reverseOrder());
        //this.invalidateOptionsMenu();
        //menu.clear();
        this.onPrepareOptionsMenu(menu);
    }

    // touch events to get manu by doubly tapping the screen.
    @Override
    public boolean onTouchEvent(MotionEvent event){
        if (this.mDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent event) {
        //Log.d(DEBUG_TAG,"onDown: " + event.toString());
        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        //Log.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());
        return true;
    }

    @Override
    public void onLongPress(MotionEvent event) {
        //Log.d(DEBUG_TAG, "onLongPress: " + event.toString());
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX,
                            float distanceY) {
        //Log.d(DEBUG_TAG, "onScroll: " + event1.toString() + event2.toString());
        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
        //Log.d(DEBUG_TAG, "onShowPress: " + event.toString());
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        //Log.d(DEBUG_TAG, "onSingleTapUp: " + event.toString());
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
        //Log.d(DEBUG_TAG, "onDoubleTap: " + event.toString());
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        //Log.d(DEBUG_TAG, "onDoubleTapEvent: " + event.toString());
        //menu.clear();
        this.onPrepareOptionsMenu(menu);
        super.onPrepareOptionsMenu(menu);
        MainActivity.this.openOptionsMenu();
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
        //Log.d(DEBUG_TAG, "onSingleTapConfirmed: " + event.toString());
        return true;
    }

    // getting the location by google
    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
            //updateUI();
        }
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Creates a callback for receiving location events.
     */
    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                showMap();
                //updateLocationUI();
            }
        };
    }

    /**
     * Uses a {
     * @link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case READ_REQUEST_CODE:
                // reading a local file containing gpx data
                switch (resultCode) {
                    case Activity.RESULT_OK:
                    // The document selected by the user won't be returned in the intent.
                    // Instead, a URI to that document will be contained in the return intent
                    // provided to this method as a parameter.
                    // Pull that URI using resultData.getData().
                    Uri uri = null;
                    String result = null;
                    if (data != null) {
                        uri = data.getData();
                        //Log.i(TAG, "Uri: " + uri.toString());
                        try {
                            result = this.readTextFromUri(uri);
                        } catch (Exception e) {
                               Log.i(TAG, "Problem in uri read");
                        }
                            //Log.i(TAG, "Uri: " + result);
                            gpxUtil.setloadMapFromWWW(false); //prevent map load when reading gpx
                            this.showMapOnly=false;
                            networkUtils.setCopyComboBitmapNull(); // new track, delete old copy
                            try{gpxUtil.decodeGpxString(result, true);}catch (Exception e){e.printStackTrace();};
                            //Log.d("MAIN:","SHOW TRACK MAP STa");
                            this.showMapWithTrack(gpxUtil.getGPXList());
                        //} catch (Exception e) {
                         //   Log.i(TAG, "Problem in gpx read");
                        //}
                    }
                    break;
                }

        // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        //Log.i(TAG, "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        //Log.i(TAG, "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        //updateUI();
                        break;
                }
                break;
        }
    }




    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    private void startLocationUpdates() {
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        //Log.i(TAG, "All location settings are satisfied.");

                        //noinspection MissingPermission
                        try {
                            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                    mLocationCallback, Looper.myLooper());
                        }catch (SecurityException e){
                            Log.i("some security problem:", "mok");
                        }
                        //updateUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                //Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                //        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                   // Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                //Log.e(TAG, errorMessage);
                                Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                mRequestingLocationUpdates = false;
                        }

                        //updateUI();
                    }
                });
    }



    /**
     * Removes location updates from the FusedLocationApi.
     */
    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            //Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                        //setButtonsEnabledState();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        mRequestingLocationUpdates=true;

        // Within {@code onPause()}, we remove location updates. Here, we resume receiving
        // location updates if the user has requested them.
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        } else if (!checkPermissions()) {
            requestPermissions();
        }

        //updateUI();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Remove location updates to save battery.
        stopLocationUpdates();
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        mRequestingLocationUpdates=true;
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            //Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            //Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        //Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                //Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mRequestingLocationUpdates) {
                    //Log.i(TAG, "Permission granted, updates requested, starting location updates");
                    startLocationUpdates();
                }
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    // stuff related to reading gpx-track from a local file
    /**
     * Fires an intent to spin up the "file chooser" UI and select an image.
     */
    public void performFileSearch() {

        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("*/*");

        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    private String readTextFromUri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }
        inputStream.close();
        reader.close();
        return stringBuilder.toString();
    }


}





