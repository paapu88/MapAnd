package com.mka.mka.mapand;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
//import javax.net.ssl.HttpsURLConnection;

/**
 *  Created by xkaukone on 29.11.2017.
 */

public class NetworkUtils extends Tiles{

    private int oneBitmapWidth;
    private int oneBitmapHeight;
    private int comboBitmapWidth;
    private int comboBitmapHeight;
    private Bitmap comboBitmap=null;
    // copy of the map+course on it,
    // this is needed when being away from the map or in other scale
    private Bitmap copyComboBitmap=null;
    final static float CONTROL_RADIUS=3.0f; // O-control is 3mm in radius
    private float control_radius;
    private boolean showTrack;         // do we show gpx track?
    private Integer nx_min, nx_max, ny_min, ny_max; // tile range for bigger map
    private int min_tiles_x, min_tiles_y; // minimum of tiles in x and y, based on display

    private boolean canDrawPoint; // if we are outside of map this is false
    private float shift_x, shift_y; // if we are on the edge of the map
    private GPXUtil gpxUtil;
    private MainActivity main;
    private boolean mapLoaded; // has the async task for map loading finished?
    private char direction; // which of 'N' 'S' 'E' ''W' is facing up

    private int z_www; // zoom level when map is loaded last time
    private int z_copy; // zoom level when big map with track is drawn

    private AsyncTask tileClient;

    public NetworkUtils(MainActivity main, GPXUtil gpxUtil){
        //zoomLevel=9;  // initial tile zoom level (9 biggest zoom)
        // mapant uses 256x256 tiles (2017)
        this.gpxUtil = gpxUtil;
        this.main = main;

        this.oneBitmapWidth=256;
        this.oneBitmapHeight=256;
        this.comboBitmapWidth=3*256;
        this.comboBitmapHeight=4*256;
        this.z_www = 9; // the zoom level the when big map is drawn
        this.z_copy = 0;

        this.showTrack = false;

        this.canDrawPoint = true;
        this.shift_x = 0.0f;
        this.shift_y = 0.0f;

        mapLoaded = false;
        nx_min = null;
        ny_min = null;
        nx_max=null;
        ny_max=null;
        direction = 'N';

        this.tileClient = null;
    }

    public void setMin_tiles_x(int min_tiles_x){
        // min tiles in x based on display of device
        this.min_tiles_x = min_tiles_x;
    }
    public void setMin_tiles_y(int min_tiles_y){
        // min tiles in y based on display of the device
        this.min_tiles_y = min_tiles_y;
    }
    public void setControlEtcSizesInPixels(double mm2Pixel){
        // also the pixel number of the map show is taken into accout
        // (this.main.myDisplay.getScreenX())
        this.control_radius = CONTROL_RADIUS*(float) mm2Pixel *
                (float) this.main.myDisplay.getxInPixels()/
                (float) this.main.myDisplay.getScreenX();
        //Log.d("NET:B "," "+this.control_radius+" "+ CONTROL_RADIUS+" "+(float) mm2Pixel);
    }

    public void setshowTrack(boolean show){
        this.showTrack = show;
    }

    public boolean getshowTrack(){
        return this.showTrack;
    }

    public void setTileSize(int x, int y, int z){
        // get the initial info from one tile
        this.oneBitmapWidth = 256;
        this.oneBitmapHeight = 256;
    }

    public void setComboBitmapNull(){
        // when a new sacle is loaded, the old scale should be forgotten
        this.comboBitmap = null;
    }

    public void setCopyComboBitmapNull(){
        // when a new track is loaded, the old copy of track should be deleted
        this.copyComboBitmap = null;
        this.z_copy = 0;
        // kill old map collecting task before new one is started
        if (tileClient != null){
            tileClient.cancel(true);
        }
    }

    public void setBigBitmap(int z){

        //Load the big map which covers the whole track/ orienteering course
        //nx_min..nx_max tile range in longitude
        //ny_min..ny_max tile range in latitude
        //z zoom level of tiles

        //This is also used to set a smaller bitmap
        //around the current position (in case there is no track/ o-course)
        //Log.d("NETTTT11:"," "+nx_min+" "+nx_max+" "+ny_min+" "+ny_max);

        // can we load the map containing the whole track/ o-course
        if (this.showTrack && !this.gpxUtil.getTooBig()) {
            // in case of the same or bigger tile we reload previously loaded map
            if (z==z_copy && copyComboBitmap!= null){
                z_www = z_copy;
                comboBitmap = copyComboBitmap;
                Log.i("NETW:","took copy");
            }
            calculateTileRangeforTrack(z); // min..max lon/lat
        }

        //Log.d("NETTTT2:"," "+nx_min+" "+nx_max+" "+ny_min+" "+ny_max);

        //Log.d("NET:", "zoom:"+z);
        this.z_www = z;   // zoomlevel when map is loaded
        //Log.d("NETTTT3:"," "+nx_min+" "+nx_max+" "+ny_min+" "+ny_max);


        String urls[] = new String[10000];
        int mapCount = 0;
        for (int i = nx_min; i <= nx_max; i++) {
            for (int j = ny_min; j <= ny_max; j++) {
                String url = "http://www.mapant.se/fi/tiles/"
                        + String.valueOf(z) + "/"
                        + String.valueOf(j) + "/"
                        + String.valueOf(i) + ".png";
                urls[mapCount]=url;
                mapCount++;
            }
        }
        // kill old map collecting task before new one is started
        if (tileClient != null){
            tileClient.cancel(true);
        }
        this.mapLoaded = false;
        this.tileClient = new TileClient(main).execute(urls);
    }

    public void drawMap(ArrayList<Integer> xx,
                        ArrayList<Integer> yy,
                        ArrayList<Bitmap> maps)
    /*
     After async task< for reading map data has finished, we draw the map
    */
    {
        //Log.d("NETTTT:"," "+maps.size());
        this.nx_min = Collections.min(xx);
        this.nx_max = Collections.max(xx);
        this.ny_min = Collections.min(yy);
        this.ny_max = Collections.max(yy);

        int nx = nx_max - nx_min + 1;
        int ny = ny_max - ny_min + 1;
       // Log.d("NET----"," "+nx+" #### "+ny+" zoom:"+this.z_www);

        comboBitmap = Bitmap.createBitmap(nx * oneBitmapWidth, ny * oneBitmapHeight,
                Bitmap.Config.ARGB_8888);
        this.comboBitmapWidth = comboBitmap.getWidth();
        this.comboBitmapHeight = comboBitmap.getHeight();

        Canvas comboImage = new Canvas(comboBitmap);

        for (int imap = 0; imap < maps.size(); imap++) {
            try {
                comboImage.drawBitmap(maps.get(imap), (xx.get(imap) - nx_min) * oneBitmapWidth,
                        (yy.get(imap) - ny_min) * oneBitmapHeight, null);
            }catch (Exception e){
                //Log.d("skipping frame:", "XX");
            }
        }

        if (this.showTrack) {
            if (gpxUtil.isItOrienteeringCourse()){
                try {
                    drawOrienteeringCourse(comboImage, Color.RED);
                } catch (Exception e){
                    //Log.d("NET: ","problem drawing orienteering course");
                }
            }
            else {
                try {
                    drawTrack(comboImage, Color.RED);
                } catch (Exception e) {
                    //Log.d("NET: ","problem drawing gpx file");
                }
            }
        }
        //Log.d("NET:"," done with map");
        this.mapLoaded = true;
        this.main.showMap();
        // if this is the first time we draw a course, make a copy
        if (copyComboBitmap==null && this.getshowTrack() && !gpxUtil.getTooBig()) {
            copyComboBitmap = comboBitmap.copy(comboBitmap.getConfig(), true);// make a copy, so that there is only one dot
            this.z_copy = z_www;
        }
    }


    public boolean getMapLoaded(){
        return this.mapLoaded;
    }

    public void setMapLoaded(boolean loaded){
        this.mapLoaded = loaded;
    }

    public int getNumberOfTiles(){
        return (ny_max - ny_min + 1) * (nx_max - nx_min + 1);
    }

    public void calculateTileRangeforTrack(int z){
        /*
        calculate the ranges in x and y for tiles under a track/ orienteering course
        leave one extra tile on both sides

        Latitudes increase when going north,
        but Y decreases when going north (the Finnish coordinate system)
        Therefore minLatitude corresponds to max Y
         */

        int[] xyz;
        xyz = degs2XYZ(gpxUtil.getMinLongitude(), gpxUtil.getMinLatitude(), z);
        nx_min = xyz[0]-1;
        ny_max = xyz[1]+1;
        xyz = degs2XYZ(gpxUtil.getMaxLongitude(), gpxUtil.getMaxLatitude(), z);
        nx_max = xyz[0]+1;
        ny_min = xyz[1]-1;
        // check for small  tracks
        if (nx_max-nx_min+1 < this.min_tiles_x){
            nx_max = nx_min + this.min_tiles_x;
        }
        if (ny_max-ny_min+1 < this.min_tiles_y){
            ny_max = ny_min + this.min_tiles_y;
        }
    }


    public void setBigBitmapNearBy(int nx, int ny, int x, int y, int z){
        /*
        draw a bitmap around current location
         */
        //Log.d("NETTTT:"," "+nx+" "+ny+" "+x+" "+y+" "+z);
        int xlow = nx/2;
        int ylow = ny/2;

        this.nx_min = x-xlow;
        this.nx_max = x+xlow;
        this.ny_min = y-ylow;
        this.ny_max = y+ylow;

        //Log.d("NETTTT:"," "+nx_min+" "+nx_max+" "+ny_min+" "+ny_max);

        this.setBigBitmap(z);
    }

    private int[] check_limits(long xp, long yp, int dx, int dy){
        // if some part of the map is outside of combomap,
        // change the limits of the drawn image
        // xxdot
        int x_left = (int) (xp - dx/2);
        int y_top = (int) (yp - dy/2);

        this.shift_x = 0.0f;
        this.shift_y = 0.0f;

        // we are totally outside of the big map
        if (((x_left + dx) < 0)
            || (x_left > comboBitmapWidth)
            || ((y_top + dy) < 0)
            || (y_top > comboBitmapHeight)){
                this.canDrawPoint = false;
        }else
        {this.canDrawPoint = true;}
        if ((x_left) < 0){
            //dx = dx + x_left;
            this.shift_x = x_left;
            x_left = 0;
        }
        else if ((x_left + dx) > comboBitmapWidth){
            this.shift_x = x_left + dx - comboBitmapWidth;
            x_left = comboBitmapWidth - dx;
        }
        if ((y_top) < 0){
            this.shift_y = y_top;
            y_top = 0;
        }
        else if ((y_top + dy) > comboBitmapHeight){
            this.shift_y = y_top + dy - comboBitmapHeight;
            y_top = comboBitmapHeight - dy;
        }
        // final check
        if (dx > comboBitmapWidth){dx = comboBitmapWidth;};
        if (dy > comboBitmapHeight){dy = comboBitmapHeight;};

        int[] corrected = {x_left, y_top, dx, dy};
        return corrected;
    }

    public Bitmap setImg(ImageView im, int x, int y, int z,
                         float xx, float yy, float dx, float dy,
                         char dire){
        /*
        Draw the map on display
        rotate it to east, west, south or north(no rotation)
        xxdot
         */
        if (dire != ' '){
            this.direction = dire;
        }

        Bitmap localMap; // the portion of the map to be displayed

        //Log.d("MAP2",comboBitmap.getWidth()+" "+comboBitmap.getHeight());
        long xp = (x - nx_min)*oneBitmapWidth+Math.round(xx*oneBitmapWidth);
        long yp = (y - ny_min)*oneBitmapHeight+Math.round(yy*oneBitmapHeight);
        //Log.d("MEGSS",x+" "+x_www+" "+ilow+" "+oneBitmapWidth);
        //Log.d("MAP:",xp+" "+yp+" "
        //        +(xp-dx/2)+" "+(yp-dy/2)+ " "+ dx+" "+dy);
        int[] corrected;
        //xxdot
        if (direction == 'N' || direction == 'S') {
            corrected = check_limits(xp, yp, (int) dx, (int) dy);
        }else{
            corrected = check_limits(xp, yp, (int) dy, (int) dx);
        }
        //Log.d("NET cor"," "+corrected[0]+" "+corrected[1]+" "+
        //        corrected[2]+" "+ corrected[3]);
        localMap = Bitmap.createBitmap(comboBitmap,
            corrected[0], corrected[1], corrected[2], corrected[3]);
        if (direction == 'S'){
            Matrix matrix = new Matrix();
            matrix.postRotate(180);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(localMap,corrected[2]
                    ,corrected[3],true);
            localMap = Bitmap.createBitmap(scaledBitmap , 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(),
                    matrix, true);
        }else if (direction == 'E'){
            Matrix matrix = new Matrix();
            matrix.postRotate(270);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(localMap,corrected[2]
                    ,corrected[3],true);
            localMap = Bitmap.createBitmap(scaledBitmap , 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(),
                    matrix, true);
        }else if (direction == 'W'){
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(localMap,corrected[2]
                ,corrected[3],true);
        localMap = Bitmap.createBitmap(scaledBitmap , 0, 0,
                scaledBitmap.getWidth(), scaledBitmap.getHeight(),
                matrix, true);
    }
        im.setImageBitmap(localMap);
        return localMap;
    }

    private float[] cutLine(float x2, float y2, float x1, float y1){
        // for nice drawing: cut lines that are inside control points
        double RAD_MULTI = 1.1; // multiply control radius by this
        float dx = x2-x1;
        float dy = y2-y1;
        double lr = Math.sqrt(dx*dx+dy*dy);
        double dxu = this.control_radius * RAD_MULTI * dx / lr;
        double dyu = this.control_radius * RAD_MULTI * dy / lr;
        float[] new_r = {(float) (x1+dxu), (float) (y1+dyu),
                (float) (x2-dxu), (float) (y2-dyu)};
        return new_r;
    }

    private void drawOrienteeringCourse(Canvas comboImage, int color) throws Exception{
        //draw orienteering course
        //Log.d("DRAW COURSE1"," "+comboImage.getWidth());
        //Log.d("DRAW COURSE2"," "+this.main.myDisplay.getxInPixels());
        Paint paint = new Paint();
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.STROKE); // no fill
        paint.setColor(color);
        float oldXTrack=0f;
        float oldYTrack=0f;
        float[] newEndPoints = new float[4];
        List<Location> locations = gpxUtil.getGPXList();
        for(int i=0; i<=locations.size(); i++){
            double longitudeTrack = locations.get(i).getLongitude();
            double latitudeTrack = locations.get(i).getLatitude();
            int[] xyz;
            xyz = degs2XYZ(longitudeTrack, latitudeTrack, z_www);
            int xTrackTile = xyz[0];
            int yTrackTile = xyz[1];
            float[] xyPercent = degs2Percentage(longitudeTrack, latitudeTrack, z_www);
            float xTrack = (xTrackTile - nx_min) * this.oneBitmapWidth +
                    xyPercent[0] * this.oneBitmapWidth;
            float yTrack = (yTrackTile - ny_min) * this.oneBitmapHeight +
                    xyPercent[1] * this.oneBitmapHeight;
            if (i==0){
                // start point, triangle not rotated prperly...
                //comboImage.drawCircle(xTrack, yTrack, this.control_radius/2, paint);
                float scale = (float) (2.0f/Math.sqrt(3.0f));
                comboImage.drawLine(
                        xTrack, yTrack-this.control_radius,
                        xTrack+this.control_radius*scale, yTrack+this.control_radius,
                        paint);
                comboImage.drawLine(
                        xTrack+this.control_radius*scale, yTrack+this.control_radius,
                        xTrack-this.control_radius*scale, yTrack+this.control_radius,
                        paint);
                comboImage.drawLine(
                        xTrack-this.control_radius*scale, yTrack+this.control_radius,
                        xTrack, yTrack-this.control_radius,
                        paint);
                oldXTrack = xTrack;
                oldYTrack = yTrack;
            }else if (i==(locations.size()-1)){
                // finish
                comboImage.drawCircle(xTrack, yTrack, this.control_radius, paint);
                comboImage.drawCircle(xTrack, yTrack, this.control_radius+0.3f*this.control_radius, paint);
                newEndPoints = cutLine(xTrack,yTrack,oldXTrack,oldYTrack);
                comboImage.drawLine(
                        newEndPoints[0], newEndPoints[1],
                        newEndPoints[2], newEndPoints[3],paint);
            }else{
                // control point
                comboImage.drawCircle(xTrack, yTrack, this.control_radius, paint);
                // line to next control/finish
                newEndPoints = cutLine(xTrack,yTrack,oldXTrack,oldYTrack);
                comboImage.drawLine(
                        newEndPoints[0], newEndPoints[1],
                        newEndPoints[2], newEndPoints[3],paint);
                oldXTrack = xTrack;
                oldYTrack = yTrack;
            }
        }
    }


    private void drawTrack(Canvas comboImage, int color) throws Exception{
        //draw track
        List<Point> points = new ArrayList<>();
        for (Location locTrack : gpxUtil.getGPXList()) {
            double longitudeTrack = locTrack.getLongitude();
            double latitudeTrack = locTrack.getLatitude();
            int[] xyz;
            xyz = degs2XYZ(longitudeTrack, latitudeTrack, z_www);
            int xTrackTile = xyz[0];
            int yTrackTile = xyz[1];
            float[] xyPercent = degs2Percentage(longitudeTrack, latitudeTrack, z_www);
            float xTrack = (xTrackTile - nx_min) * this.oneBitmapWidth +
                    xyPercent[0] * this.oneBitmapWidth;
            float yTrack = (yTrackTile - ny_min) * this.oneBitmapHeight +
                    xyPercent[1] * this.oneBitmapHeight;
            //Log.d("NET:::", String.valueOf(xTrack) + " " + String.valueOf(yTrack));
            Point point = new Point((int) xTrack, (int) yTrack);
            points.add(point);
            //comboImage.drawCircle(xTrack, yTrack, trackRadius, paint);
        }

        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3);
        paint.setColor(color);
        Path path = new Path();
        boolean first = true;
        for (int i = 0; i < points.size(); i += 2) {
            Point point = points.get(i);
            if (first) {
                first = false;
                path.moveTo(point.x, point.y);
            } else if (i < points.size() - 1) {
                Point next = points.get(i + 1);
                path.quadTo(point.x, point.y, next.x, next.y);
            } else {
                path.lineTo(point.x, point.y);
            }
        }

        comboImage.drawPath(path, paint);
    }

    public void setPoint(Bitmap map, ImageView im, float dx, float dy, char dire) {
        /*
        Draw a dot at the current location
        xxdot
        */
        if (dire != ' '){
            this.direction = dire;
        }

        Bitmap lMap = map.copy(map.getConfig(), true);  // make a copy, so that there is only one dot
        Canvas canvas = new Canvas(lMap);
        //int color = Color.WHITE;


        //canvas.drawCircle(xp, yp, radius, paint);
        //we always update map,so location is always in the middle
        // except when close to the edge of the map (shift applied then)
        //Log.d("NET "," "+ direction);
        //Log.d("NETB ", shift_x+" "+shift_y+ " "+dx+" "+dy);
        if (this.canDrawPoint) {


            float xcenter=0.0f;
            float ycenter=0.0f;
            if (direction == 'N') {
                xcenter=this.shift_x + dx / 2.0f;
                ycenter=this.shift_y + dy / 2.0f;
                //canvas.drawCircle(this.shift_x + dx / 2.0f, this.shift_y + dy / 2.0f,
                //        radius, paint);
            } else if (direction == 'S') {
                xcenter=-this.shift_x + dx / 2.0f;
                ycenter=-this.shift_y + dy / 2.0f;
                //canvas.drawCircle(-this.shift_x + dx / 2.0f, -this.shift_y + dy / 2.0f,
                //        radius, paint);
            } else if (direction == 'E') {
                xcenter= this.shift_y + dx / 2.0f;
                ycenter=-this.shift_x + dy / 2.0f;
                //canvas.drawCircle(this.shift_y + dx / 2.0f, -this.shift_x + dy / 2.0f,
                //        radius, paint);
            } else if (direction == 'W') {
                xcenter=-this.shift_y + dx / 2.0f;
                ycenter=this.shift_x + dy / 2.0f;
                //canvas.drawCircle(-this.shift_y + dx / 2.0f, this.shift_x + dy / 2.0f,
                //        radius, paint);
            }
            Paint paint = new Paint();
            //paint.setColor(color);
            int color = Color.RED;
            paint.setColor(color);
            float radius = this.control_radius / 2;

            if (this.main.myDisplay.getMapScale() > 0.0001) {
                radius = this.control_radius / 2 + 6;
            }
            canvas.drawCircle(xcenter, ycenter, radius, paint);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(4); // set stroke width
            paint.setColor(Color.parseColor("#FFFF00")); // set stroke color
            canvas.drawCircle(xcenter, ycenter, radius, paint);
            im.setImageBitmap(lMap);
        }
    }

    //https://stackoverflow.com/questions/44309241/warning-this-asynctask-class-should-be-static-or-leaks-might-occur
    static class TileClient extends AsyncTask<String, Integer, String> {
        static ArrayList<Integer> xx = new ArrayList<Integer>();
        static ArrayList<Integer> yy = new ArrayList<Integer>();
        static ArrayList<Bitmap> maps =  new ArrayList<Bitmap>();

        private WeakReference<MainActivity> activityReference;
        // only retain a weak reference to the activity
        TileClient(MainActivity context) {
            activityReference = new WeakReference<>(context);
        }
        //private WeakReference<NetworkUtils> thisReference;
        //// only retain a weak reference to the activity
        //TileClient(NetworkUtils context) {
        //    thisReference = new WeakReference<>(context);
        //}


        public Bitmap getBitmap(URL url){
            //get bitmap from the given url
            Bitmap bm=null;
            try {
                InputStream is = (InputStream) url.getContent();
                bm = BitmapFactory.decodeStream(is);
                is.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bm;
        }
        @Override
        protected void onPreExecute() {
            MainActivity activity = activityReference.get();
            ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.progressBar);
            progressBar.setMax(100);
            progressBar.setVisibility(View.VISIBLE);
            TextView txt = (TextView) activity.findViewById(R.id.progText);
            if (activity.showMapOnly || activity.gpxUtil.getTooBig()){
                txt.setText(R.string.load_map_nearby);
            }else {
                txt.setText(R.string.load_whole_map);
            }
            txt.setVisibility(View.VISIBLE);
        }
        @Override
        public String doInBackground(String... urls) {
            MainActivity mainActivity = activityReference.get();
            //NetworkUtils netActivity = thisReference.get();
            //Log.d("ASYNC1","Start");
            // get max
            xx.clear();
            yy.clear();
            maps.clear();
            int count=0;
            for (String urlString:urls){
                URL url = null;
                 if (urlString==null) {
                     break;
                 }
                 count++;
            }
            int nTiles=count;
            count=0;

            for (String urlString:urls){
                if (urlString==null){
                    break;
                }
                //Log.d(TAG,urlString);
                publishProgress((int)(100*count/nTiles));
                count++;
                Bitmap oneBitmap;
                try{
                    URL url = new URL(urlString);
                    oneBitmap = getBitmap(url);
                }catch (Exception e){
                    // when map data is missing, do nothing
                    //Log.d("NET:::","missing map data, ASYNC "+urlString);
                    oneBitmap = null;
                    /*
                    oneBitmap = Bitmap.createBitmap(
                            (int)mainActivity.networkUtils.getTileSize(),
                            (int)mainActivity.networkUtils.getTileSize(),
                            Bitmap.Config.ARGB_4444);
                    Canvas canvas = new Canvas(oneBitmap);
                    Paint paint = new Paint(Color.BLACK);
                    paint.setAlpha(0);
                    canvas.drawBitmap(oneBitmap, 0, 0, paint);
                    */
                }

                String[] xyzs = urlString.split("/");
                String oneX = xyzs[7].split("\\.")[0];
                //Log.d("Async:",urlString);
                //Log.d("Async:",oneX+" "+xyzs[6]);
                xx.add(Integer.valueOf(oneX));
                yy.add(Integer.valueOf(xyzs[6]));
                maps.add(oneBitmap);
                //Log.d(TAG,urlString);
            }
            return "OK";
        }
        @Override
        protected void onPostExecute(String result) {
            //Log.d("POST::",result);
            MainActivity mainActivity = activityReference.get();
            //NetworkUtils netActivity = thisReference.get();
            try{
                mainActivity.networkUtils.drawMap(xx, yy, maps);
            }catch (Exception e){};

            ProgressBar progressBar = (ProgressBar) mainActivity.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.INVISIBLE);
            TextView txt = (TextView) mainActivity.findViewById(R.id.progText);
            txt.setVisibility(View.INVISIBLE);
            mainActivity.networkUtils.setMapLoaded(true);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            MainActivity mainActivity = activityReference.get();
            ProgressBar progressBar = (ProgressBar) mainActivity.findViewById(R.id.progressBar);
            progressBar.setProgress(values[0]);
        }
    }


}

