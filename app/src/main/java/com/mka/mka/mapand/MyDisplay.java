package com.mka.mka.mapand;

import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import static java.lang.Math.max;

/**
 * Created by mka on 26.12.2017.
 */

public class MyDisplay {
    /*
    Methods related to display of the device
     */
    int zoomLevel;
    Display display;
    DisplayMetrics dm;
    WindowManager windowManager;
    double mapScale;
    //android.view.Display display;
    double xOnMap, yOnMap; //how many meters is our display in x and y
    double xInNature, yInNature; // how many meters is our display in naute in x and y
    Tiles tiles;
    double xInPixels, yInPixels; // how many pixels we need from mapant
    int nx, ny; // number of tiles in x and y
    int screenX, screenY; // screen dimensions in pixels

    public MyDisplay(Display display, DisplayMetrics dm, double mapScale) {
        this.display = display;
        this.dm = dm;
        this.mapScale = mapScale;   // scale of the map
        tiles = new Tiles();  // tiles related methods
        this.setZoomLevel();    // zoom level dictated by scale of the map
        this.updateSettings();
    }

    private void setZoomLevel(){
        if (this.mapScale > 0.00009){ // 1:4000, 1:10000
            this.zoomLevel = 9;
        }else if (this.mapScale > 0.00003) { // 1:15000, 1:20000
            this.zoomLevel = 8;
        }
        else if (this.mapScale > 0.00001) { // 1:50000
            this.zoomLevel = 6;
        }
        else {
            this.zoomLevel = 4; // 1:200000
        }
        this.updateSettings();
    }

    private void updateSettings(){
        this.setXYOnDisplay();
        this.setXYInNature();
        this.setXYPixels();
        // make number of tiles equal (we can autorotate...)
        this.setNumberOfTilesX();
        this.setNumberOfTilesY();
        ny=max(ny, nx);
        nx=max(ny,nx);
    }

    private void setXYOnDisplay() {
        // how many meters is our display in x and y
            try {
                Point realSize = new Point();
                display.getRealSize(realSize);
                //Log.d("REALSIZE:", String.valueOf(realSize.x) + " " + String.valueOf(realSize.y));
                xOnMap = realSize.x / (double) (dm.densityDpi) * 2.54d * 0.01d;
                yOnMap = realSize.y / (double) (dm.densityDpi) * 2.54d * 0.01d;

                this.screenX = realSize.x;
                this.screenY = realSize.y;
            } catch (Exception ignored) {
                //Log.d("ERR:"," E");
                ignored.printStackTrace();
            }
        //Log.d("MYD: MAP", String.valueOf(xOnMap)+" "+String.valueOf(yOnMap));
    }

    private void setXYInNature() {
        xInNature = xOnMap / mapScale;
        yInNature = yOnMap / mapScale;
        //Log.d("MYD: NATURE", String.valueOf(xInNature)+" "+String.valueOf(yInNature));
    }

    public int getZoomLevel(){
        return this.zoomLevel;
    }

    // the size of x dimension (width) of the display in nature
    public double getXInNature() {
        return this.xInNature;
    }

    // the size of y dimension (height) of the display in nature
    public double getYInNature() {
        return this.yInNature;
    }


    // the size of x dimension width of the physical screen in pixels
    public double getScreenX() {
        return this.screenX;
    }

    private void setXYPixels() {
        // how many pixels we need from mapant
        xInPixels = (double) (xInNature * tiles.getResolution(this.zoomLevel));
        yInPixels = (double) (setAspectRatio(xInPixels));
        //Log.d("MYD: PIX", String.valueOf(xInPixels)+" "+String.valueOf(yInPixels));
    }

    public double getxInPixels(){
        return this.xInPixels;
    }

    public double getyInPixels(){
        return this.yInPixels;
    }

    public double getWholeMapInNatureX(){
        return ((double) this.nx * (double) tiles.getTileSize()/tiles.getResolution(this.zoomLevel));
    }

    public double getWholeMapInNatureY(){
        return ((double) this.ny * (double) tiles.getTileSize()/tiles.getResolution(this.zoomLevel));
    }

    private void setNumberOfTilesX() {
        /*
        Set the correct number of tiles based on physical scale of the device in x
        always an odd number, minimum is 5
         */
        // stack overflow if ( (x & 1) == 0 ) { even... } else { odd... }
        nx =  (int) Math.floor(xInPixels / tiles.getTileSize()) + 1;
        if ( (nx & 1) == 0 ) // if even, make it odd
        {
            nx = nx + 1;
            nx = max(5, nx);
        }
            else { nx  =  max(5, nx);
        }
        // nx =2;
    }

    private void setNumberOfTilesY() {
        /*
        Set the correct number of tiles based on physical scale of the device in y
        always an odd number, minimum is 5
         */
        // stack overflow if ( (x & 1) == 0 ) { even... } else { odd... }
        ny =  (int) Math.floor(yInPixels / xInPixels * nx);
        if ( (ny & 1) == 0 ) // if even make it odd
        {
            ny = ny + 1;
            ny = max(5, ny);
        }
        else { ny = max(5, ny); }
        // ny=2;
    }

    public int getNumberOfTilesX() {
        return this.nx;
    }

    public int getNumberOfTilesY() {
        return this.ny;
    }

    private double setAspectRatio(double x) {
        /*
        Get the number of tiles, number of x-tiles is given
        and y-tiles is given by aspect ratio
         */
        //double ratio = ((double) dm.heightPixels / (double) dm.widthPixels);
        //if (Build.VERSION.SDK_INT >= 17) {
            //try {
        Point realSize = new Point();
        display.getRealSize(realSize);
        //Log.d("SIZE:", String.valueOf(realSize.x) + " " + String.valueOf(realSize.y));
        double ratio = ((double) realSize.y / (double) realSize.x);
        //} catch (Exception ignored) {
        //}
        //}
        return x * ratio;
    }

    public double getMilliMeterToPixel(){
        //how many pixels is one millimeter on the screen
        //xOnMap = (double) (dm.widthPixels) /
        //Log.d("MET:A "," "+this.zoomLevel+" "+dm.widthPixels+" "+this.xOnMap);
        return (dm.widthPixels)/(this.xOnMap*1000);
    }

    public double getMapScale(){
        return this.mapScale;
    }
}