package com.mka.mka.mapand;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static android.content.Context.ACTIVITY_SERVICE;
import static java.lang.Math.min;

/**
   Created by mka on 23.12.2017.
 */

public class GPXUtil {
    // where to find gpx files:

    public final float MIN_DIST=20;   // distance in loc queue [m]
    public final float MAX_DIST=500;   // distance in loc queue [m]

    public final String[] GPX_FOLDER_NAMES = {"/GPXMapAnd","/Download","/GPX Logger"};
            //Environment.getExternalStorageDirectory().toString()+"GpxFolder";

    private Location startLocation;  // starting point for gpx-track orr an orienteering course
    private List<Location> gpxList; // list containing track coordinates
    private ArrayList<String> gpxFileNames; // all files in given folders ending with gpx
    private String gpxFileName; //the name of the file of the selected track/course
    private double min_longitude, max_longitude;
    private double min_latitude, max_latitude;

    private boolean loadMapFromWWW; // can we start downloading maps?
    private LinkedList<Location> queue;
    private static int MAX_LOCS = 20;  // max number of last locations to determine direction
    private static int MIN_LOCS = 3;
    private boolean tooBig;  // map is too big to be loaded in one piece
    private boolean isOcourse;

    private Double[] oldDirectionVector;
    private static Context context;

    // for toasts: public GPXUtil(Context c){
    public GPXUtil(){
        //this.createDirForGPXFiles();
        // for toasts this.context = c; // to show toasts
        gpxFileNames = new ArrayList<String>();
        gpxFileName = null;
        this.min_longitude = 500d;
        this.max_longitude = -500d;
        this.min_latitude = 500d;
        this.max_latitude = -500d;
        this.loadMapFromWWW = false;   // can we load map from internet?
        queue = new LinkedList<Location>();  // max x last locations
        tooBig = true;
        this.isOcourse = false;
        oldDirectionVector = null;
        this.startLocation = null;
    }


    public void setloadMapFromWWW(boolean loadMapFromWWW){this.loadMapFromWWW = loadMapFromWWW;}
    //public boolean getloadMapFromWWW(){return this.loadMapFromWWW;}
    //public void setgpxFileName(String name){this.gpxFileName = name;}

    public String getGpxFileName(){
        return gpxFileName;
    }
    public boolean getIsOcourse() {return this.isOcourse; }

    public void resetPositionQueue(){
        while (!this.queue.isEmpty()) {
            this.queue.removeFirst();
        }
    }

    public void updatePositionQueue(Location loc){
        //System.out.println("loc in set:"+ loc.getLongitude()+ " :"+loc.getLatitude());

        this.queue.add(loc);

        if (this.queue.size() > MAX_LOCS){
            Location l = this.queue.removeFirst();
        }
    }


    public Queue<Location> getQueue(){
        return this.queue;
    }

    public Double[] getDirectionVector(){
        //https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average
        double WEIGHT = 0.8;

        //Log.d("GPX queue"," "+this.queue.size());
        if (this.queue == null || this.queue.size()< MIN_LOCS) {
            //System.out.println("RETURNING NULL  "+this.queue.size());
            Double[] new_r = null;
            return new_r;
        }else{
            Location oldLoc = null;
            Location loc1 = null;
            Location loc2 = null;
            double sumx=0;
            double sumy=0;

            for(Location loc: this.queue) {
                if (loc1 == null) {
                    loc1 = loc;
                } else {
                    loc2 = loc;
                    //DO IT
                    double dx, dy;
                    dx = loc2.getLongitude() - loc1.getLongitude();
                    dy = loc2.getLatitude() - loc1.getLatitude();
                    //Log.d("DIRECTION"," "+ dx + " " + dy);
                    //System.out.println("lon1:"+ loc1.getLongitude()+ " lon2:"+loc2.getLongitude());
                    //system.ouSystem.out.println("dlon:"+ dx+ " dlat:"+dy);

                    sumx = WEIGHT * dx + (1 - WEIGHT) * sumx;
                    sumy = WEIGHT * dy + (1 - WEIGHT) * sumy;
                    loc1 = loc2;
                }
            }

            Double[] new_r = new Double[]{sumy, sumx};
            //System.out.println("DIRECTION SUM"+ new_r[0]+ " "+new_r[1]);
            //Log.i("DIRECTION SUM:"," "+ new_r[0]+ " "+new_r[1]);
            return new_r;
        }
    }

    public char checkDirection(char oldDirection){
        // returns old direction if old and new direction are about the same
        // also returnsold direction if last and first Locations in the
        // Location queue are about the same (we are not moving)

        if (queue.size() < MIN_LOCS){
            //Toast.makeText(this.context, "too few points", Toast.LENGTH_LONG).show();
            return oldDirection;
        }

        double minDist = 20; // one must move 20m in X seconds
        // otherwise return new direction
        Double[] r = this.getDirectionVector();

        if (queue.getFirst().distanceTo(queue.getLast()) < minDist){
            //Toast.makeText(this.context, "first-l close:"+queue.getFirst().distanceTo(queue.getLast()), Toast.LENGTH_LONG).show();

            return oldDirection;
        }
        double angle1_rad = Math.atan2(r[0], r[1]);
        //System.out.println("Angle rad:"+angle1_rad);
        if (oldDirectionVector != null) {
            double angle2_rad = Math.atan2(oldDirectionVector[0], oldDirectionVector[1]);
            if ((Math.abs(angle1_rad - angle2_rad)) < Math.PI / 4.0) {
                // about the same direction
                return this.getNewDirection(oldDirection, Math.PI / 4.5); // this leaves 10deg as gray area between 'N' 'E' 'S' 'W'
            } else { //totally other direction
                return this.getNewDirection(oldDirection, Math.PI / 4.0);
            }
        }else {
            oldDirectionVector=r;
            return this.getNewDirection(oldDirection, Math.PI / 4.0);
        }
    }

    private char getNewDirection(char oldDirection, double mydeg) {
        // mydeg measures how big is the gray area

        Double[] r = this.getDirectionVector();
        //Log.i("GPXDIREc:", " "+r[0]+" "+r[1]);
        if (r == null) {
            //Toast.makeText(this.context, "Dir vect ZERO", Toast.LENGTH_LONG).show();
            return oldDirection;
        }
        double angle_rad = Math.atan2(r[0], r[1]);
        // System.out.println("RAD"+angle_rad);
        //double mydeg = Math.PI / 36.0;  // gray area between N, W, E, S
        char newDirection = oldDirection;
        if (angle_rad < (-Math.PI / 2 + mydeg) && angle_rad > (-Math.PI / 2 - mydeg)) {
            newDirection = 'S';
        } else if (angle_rad > (Math.PI - mydeg) || angle_rad < (-Math.PI + mydeg)) {
            newDirection = 'W';
        } else if (angle_rad < (Math.PI / 2 + mydeg) && angle_rad > (Math.PI / 2 - mydeg)) {
            newDirection = 'N';
        } else if (angle_rad < mydeg && angle_rad > -mydeg) {
            newDirection = 'E';
        }
        //if (oldDirection != newDirection){
        //    Toast.makeText(this.context, "changing to " + newDirection, Toast.LENGTH_LONG).show();
        //}
        return newDirection;
    }

/*
    private void createDirForGPXFiles(){
        // create a folder to store gpx tracs which can be show on the map
        // create a File object for the parent directory
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), GPX_FOLDER_NAMES[0]);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                //Log.d("App", "failed to create directory");
            }
        }
    }
*/


    public List<Location> getGPXList(){
        String s="";
        for(Location loc: this.gpxList){
            //Log.d("GPX:G:",loc.toString());
            s=s+loc.toString();
        }
        //Toast.makeText(this.context, s, Toast.LENGTH_LONG).show();

        return this.gpxList;
    }
    public void setGPXList(List<Location> gpxList){
        this.gpxList = gpxList;
    }

    public void readGPXTrack(String fileName) throws Exception{
        boolean getWholeList = true;
        gpxList = decodeGPX(fileName, getWholeList);
    }

    public boolean isItOrienteeringCourse(){
        // calculate the average distance between first 10 gpx points
        // if the average distance between points is more than 60m
        // we classify the track as an orienteering course
        if (this.gpxList == null){
            //Log.d("GPXutil","NO TRACK for orienteering check");
            System.exit(0);
        }
        if (this.gpxList.size() > 60){
            this.isOcourse = false;
            return false;
        }
        else{
            this.isOcourse = true;
            return true;
        }
        /*
        int count_max = min(50, this.gpxList.size()-1);
        float dist=0.0f;
        int count=0;
        for (int i=0; i<count_max; i++){
            dist = dist + gpxList.get(i).distanceTo(gpxList.get(i+1));
            count++;
            }
        dist = dist / count;
        //Log.d("GPX: aver dist", dist+" "+count_max);

        if (dist > 60){
            this.isOcourse = true;
            return true;
            //return false;
        }else{
            this.isOcourse = false;
            return false;
        }

         */
    }


    public boolean isTrackTooBig(int nTiles, long memoryinMegs){
        long memoryNeeded = nTiles*256*256*4*8/1048576L;
        if (memoryNeeded > memoryinMegs*0.8){
            Log.d("GPX","CANNOT LOAD THE WHOLE MAP..."+memoryNeeded+" "+memoryinMegs+" "+nTiles);
            this.tooBig = true;
            return true;
        }else{
            Log.d("GPX","LOADING THE WHOLE MAP..."+memoryNeeded+" "+memoryinMegs+ " "+nTiles);
            this.tooBig = false;
            return false;
        }
    }

    public boolean getTooBig(){
        return this.tooBig;
    }

    public void decodeGpxString(String gpxString, boolean getWholeList){
        // parse gpx data from a string
        final String LAT_START = " lat=\"";
        final String LON_START = " lon=\"";
        final String STOP ="\"";
        List<Location> list = new ArrayList<>();
        List<Double> longitudes = new ArrayList<>();
        List<Double> latitudes = new ArrayList<>();

        int fromIndex=0;
        int slen = gpxString.length();
        int count = 0;
        //Log.d("GPX","START GPX READ");
        while (fromIndex < slen){
            //Log.d("GPX","R0 "+fromIndex);
            int ilat_start = gpxString.indexOf(LAT_START, fromIndex)+LAT_START.length();
            int ilat_stop = gpxString.indexOf(STOP, ilat_start);
            int ilon_start = gpxString.indexOf(LON_START, fromIndex)+LON_START.length();
            int ilon_stop = gpxString.indexOf(STOP, ilon_start);
            //Log.d("GPX","R2 "+ilat_start+" "+ilat_stop+" "+ilon_start+" "+ilon_stop);
            if (ilat_start < fromIndex || ilat_stop < fromIndex || ilon_start < fromIndex || ilon_stop < fromIndex){break;};
            if (ilat_start==-1 || ilon_start==-1 || ilat_stop==-1 || ilon_stop==-1) {break;};
            String slat = gpxString.substring(ilat_start, ilat_stop);
            String slon = gpxString.substring(ilon_start, ilon_stop);
            //Log.d("GPX","R3 "+slat+" "+slon);
            Double newLatitude_double = Double.parseDouble(slat);
            Double newLongitude_double = Double.parseDouble(slon);
            String newLocationName = String.valueOf(count) + ":" + String.valueOf(count);
            Location newLocation = new Location(newLocationName);
            newLocation.setLatitude(newLatitude_double);
            newLocation.setLongitude(newLongitude_double);

            list.add(newLocation);
            longitudes.add(newLongitude_double);
            latitudes.add(newLatitude_double);
            count++;
            fromIndex = ilon_stop + 1;
            //Log.d("GPX from"," "+fromIndex+" "+slen);
        }
        this.min_longitude = Collections.min(longitudes);
        this.max_longitude = Collections.max(longitudes);
        this.min_latitude = Collections.min(latitudes);
        this.max_latitude = Collections.max(latitudes);
        this.loadMapFromWWW = true; // track is ok, we can now do the map
        this.startLocation = list.get(0);
        this.setGPXList(list);
    }


    public List<Location> decodeGPX(String fileName, boolean getWholeList){
        // read gpx track to a list of location objects

        //new File(Environment.getExternalStorageDirectory(), GpxFolderName);
        File file = new File(fileName);
        //File file = new File(Environment.getExternalStorageDirectory()+"/"+
        //        this.GpxFolderName+"/"+fileName);
        List<Location> list = new ArrayList<>();
        List<Double> longitudes = new ArrayList<>();
        List<Double> latitudes = new ArrayList<>();

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            FileInputStream fileInputStream = new FileInputStream(file);
            Document document = documentBuilder.parse(fileInputStream);
            Element elementRoot = document.getDocumentElement();

            NodeList nodelist_trkpt = elementRoot.getElementsByTagName("trkpt");

            for(int i = 0; i < nodelist_trkpt.getLength(); i++){

                Node node = nodelist_trkpt.item(i);
                NamedNodeMap attributes = node.getAttributes();

                String newLatitude = attributes.getNamedItem("lat").getTextContent();
                Double newLatitude_double = Double.parseDouble(newLatitude);

                String newLongitude = attributes.getNamedItem("lon").getTextContent();
                Double newLongitude_double = Double.parseDouble(newLongitude);

                String newLocationName = newLatitude + ":" + newLongitude;
                Location newLocation = new Location(newLocationName);
                newLocation.setLatitude(newLatitude_double);
                newLocation.setLongitude(newLongitude_double);

                list.add(newLocation);
                longitudes.add(newLongitude_double);
                latitudes.add(newLatitude_double);

                // sometimes we only need the first location
                if (!getWholeList){break;}

            }
            this.min_longitude = Collections.min(longitudes);
            this.max_longitude = Collections.max(longitudes);
            this.min_latitude = Collections.min(latitudes);
            this.max_latitude = Collections.max(latitudes);
            fileInputStream.close();
        } catch (ParserConfigurationException e) {
            // Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            //
            e.printStackTrace();
        } catch (SAXException e) {
            //
            e.printStackTrace();
        } catch (IOException e) {
            // Auto-generated catch block
            e.printStackTrace();
        }
        this.loadMapFromWWW = true; // track is ok, we can now do the map
        this.startLocation = list.get(0);
        return list;
    }

    public Location getStartLocation(){
        return this.startLocation;
    }

    public void setMaxMinLatLon(List<Location> gpxList){
        List<Double> longitudes = new ArrayList<>();
        List<Double> latitudes = new ArrayList<>();
        for(Location loc:gpxList){
            latitudes.add(loc.getLatitude());
            longitudes.add(loc.getLongitude());
        }
        this.min_longitude = Collections.min(longitudes);
        this.max_longitude = Collections.max(longitudes);
        this.min_latitude = Collections.min(latitudes);
        this.max_latitude = Collections.max(latitudes);
    }

    public Double getMinLongitude(){
        return this.min_longitude;
    }
    public Double getMaxLongitude(){
        return this.max_longitude;
    }
    public Double getMinLatitude(){
        return this.min_latitude;
    }
    public Double getMaxLatitude(){
        return this.max_latitude;
    }


    public void scan(File f) {
        //Log.d("GPX-file:",f.toString());
        File[] file = f.listFiles();

        for (File ff : file) {
            //Log.d("GPX-file2:",ff.toString());
            if (ff.isFile() && ff.getPath().endsWith(".gpx")) {
                gpxFileNames.add(ff.toString());
                //Log.d("DB Debug", ff.toString());
            }
        }
    }

    public ArrayList<String> getgpxFileNames() {
        String root = Environment.getExternalStorageDirectory().getAbsoluteFile().toString();

        gpxFileNames.clear();
        for (String s:this.GPX_FOLDER_NAMES){
            try{
                File dir = new File(root+s);
                this.scan(dir); // putting file names to gpxFileNames
            }catch(Exception e){
                //Log.d("Problem reading dir:",s);
            }
        }
        return gpxFileNames;
    }


    public void getCourseNames(MainActivity main) {
        String result_string="";
        //Log.d("GET COURSES MAIN:","out");
        try {
            new HttpsClient(main).execute("https://map-and-db.herokuapp.com/tracks").execute();
        }catch(Exception e){
            e.printStackTrace();
        }
        //for (String s: result) {
        //Log.d("GET    C N:", result_string);
        //}
    }

    public void readHerokuTrack(MainActivity main, String gpxHerokuName) {
        String result_string="";
        //Log.d("GET COURSES MAIN:","out");

        try {
            new ReadHerokuTrack(main).execute("https://map-and-db.herokuapp.com/track/"+gpxHerokuName.split("_")[1]).execute();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public List<FileDistance> getNearestTracks(Location currentLoc) {
        /*
        Search all files ending with GPX and adding them to database
        Files from which one cannot get the initial longitude and latitude
        are NOT added to the database
         */
        // get all files ending with gpx
        List<Location> l;
        String root = Environment.getExternalStorageDirectory().getAbsoluteFile().toString();
        gpxFileNames.clear();
        for (String s:this.GPX_FOLDER_NAMES){
            try{
                File dir = new File(root+s);
                this.scan(dir); // putting file names to gpxFileNames
            }catch(Exception e){
                //Log.d("Problem reading dir:",s);
            }
        }

        // not yet sorted
        List<FileDistance> filesByDistance = new ArrayList<FileDistance>();

        for (String s: gpxFileNames) {
            l = decodeGPX(s, false);
            FileDistance fd = new FileDistance(s, currentLoc.distanceTo(l.get(0)));
            filesByDistance.add(fd);
        }

        //sort
        Collections.sort(filesByDistance, new Comparator<FileDistance>() {

            public int compare(FileDistance o1, FileDistance o2) {
                return (int) (Math.round(o1.getDistance() - o2.getDistance()));
            }
        });

        return filesByDistance;
    }


    /*
 * Reading track names from heroku database
 */
    static class HttpsClient extends AsyncTask<String, Void, String> {
        private Exception exception;
        private String TAG = "GET COURSES:";
        private String courses = "";
        private MainActivity mainActivity;

        public HttpsClient(MainActivity main) {
            super();
            this.mainActivity = main;
        }

        @Override
        public String doInBackground(String... urls) {

            String response=null;
            URL url = null;
            HttpsURLConnection connection = null;
            try{url = new URL(urls[0]);} catch (Exception e) { }
            try{connection = (HttpsURLConnection)url.openConnection();} catch (Exception e)
            {};
            connection.setReadTimeout(20000);
            connection.setConnectTimeout(20000);
            try{connection.setRequestMethod("HEAD");} catch (Exception e){}
            try{connection.setRequestMethod("GET");} catch (Exception e){}
            //connection.setUseCaches(false);
            //connection.setAllowUserInteraction(false);
            //connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            int responceCode = 0;
            try{responceCode = connection.getResponseCode();} catch (Exception e)
            {}
            BufferedReader br = null;
            if (responceCode == HttpURLConnection.HTTP_OK)
            {
                String line=null;
                try{
                    br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                } catch (Exception e) { }
                try{
                    line = br.readLine();
                } catch (Exception e) {line=null; }
                while (line != null)
                {
                    response = "";// String variable declared global
                    response += line;
                    //Log.i("response_line", response);
                    try{
                        line = br.readLine();
                    } catch (Exception e) {line=null; }
                }
                JSONObject obj = null;
                JSONArray geodata = null;
                try {
                    obj = new JSONObject(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    geodata = obj.getJSONArray("tracks");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (geodata != null) {
                    final int n = geodata.length();
                    for (int i = 0; i < n; ++i) {
                        JSONObject course = null;
                        try {
                            course = geodata.getJSONObject(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            //Log.d("TR:", course.getString("name"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            //Log.d("TR:", course.getString("date"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            courses = courses + course.getString("date") + "_" + course.getString("name") + " ";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            else
            {
                courses = "";
            }


            return courses;
        }

        @Override
        protected void onPostExecute(String result) {
            //Log.d("POST::", result);
            mainActivity.setCourseNames(result);
        }
    }


    static class ReadHerokuTrack extends AsyncTask<String, Void, String> {
        private Exception exception;
        private String TAG="GET GPX from HEROKU:";
        private String courses = "";
        private MainActivity mainActivity;
        String gpxData = "";
        public ReadHerokuTrack(MainActivity main){
            super();
            this.mainActivity = main;
        }
        @Override
        public String doInBackground(String... urls) {
            URL url=null;
            HttpsURLConnection con = null;

            InputStream in = null;
            BufferedReader br = null;
            String lines = "";
            //Log.d(TAG, "*******************    Open Connection    *****************************");
            try {
                url = new URL(urls[0]);
            }catch(Exception e){

            }

            //Log.d(TAG, "Received URL:  ");
            try {
                con = (HttpsURLConnection) url.openConnection();
                con.setReadTimeout(10000);
                con.setConnectTimeout(10000);
                con.setUseCaches(false);
                con.setAllowUserInteraction(false);
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            }catch(Exception e){

            }
            //Log.d(TAG, "Con Status: " + con);


            try {
                in = con.getInputStream();
            }catch(Exception e){


            }
            //Log.d(TAG, "GetInputStream:  " + in);

            //Log.d(TAG, "*******************    String Builder     *****************************");
            String line = null;

            try{
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            }catch(Exception e){

            }

            try {
                line = br.readLine();
            }catch(Exception e){
                line=null;
            }
            while (line  != null) {
                //Log.d(TAG, line);
                lines = lines + line;
                try {
                    line = br.readLine();
                }catch(Exception e){
                    line=null;
                }
            }
            // Closing the stream
            //Log.d(TAG, "*******************  Stream closed, exiting     ******************************");
            try{br.close();}catch(Exception e){e.printStackTrace();}
            JSONObject obj=null;
            JSONObject course=null;
            try{
                obj = new JSONObject(lines);
            }catch(Exception e)
            {e.printStackTrace();}
            try{
                gpxData = obj.getString("track");
            }catch(Exception e)
            {e.printStackTrace();}

            //Log.d(TAG+"MOKMOK:",gpxData);
            return gpxData;
        }

        @Override
        protected void onPostExecute(String result) {
            //Log.d("POST::",result);
            List<Location> list = new ArrayList<Location>();
            ArrayList<String> locationListStr = new ArrayList<String>(Arrays.asList(result.split(" ")));
            //Log.d("POST::"," "+locationListStr.size());

            for (int i=0; i<locationListStr.size(); i=i+2){
                //Log.d("GPX LAT",locationListStr.get(i));
                //Log.d("GPX LON",locationListStr.get(i+1));

                Location loc = new Location(locationListStr.get(i));
                loc.setLatitude(Double.parseDouble(locationListStr.get(i)));
                loc.setLongitude(Double.parseDouble(locationListStr.get(i+1)));
                list.add(loc);
            }
            mainActivity.gpxUtil.setGPXList(list);
            mainActivity.showMapWithTrack(list);
        }
    }

}
