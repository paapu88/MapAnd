package com.mka.mka.mapand;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static java.lang.Math.min;

/**
   Created by mka on 23.12.2017.
 */

public class MockGPXUtil {
    // where to find gpx files:

    public final float MIN_DIST=20;   // distance in loc queue [m]
    public final float MAX_DIST=500;   // distance in loc queue [m]

    public final String[] GPX_FOLDER_NAMES = {"/GPXMapAnd","/Download","/GPX Logger"};
            //Environment.getExternalStorageDirectory().toString()+"GpxFolder";

    private MockLocation startMockLocation;  // starting point for gpx-track orr an orienteering course
    private List<MockLocation> gpxList; // list containing track coordinates
    private ArrayList<String> gpxFileNames; // all files in given folders ending with gpx
    private String gpxFileName; //the name of the file of the selected track/course
    private double min_longitude, max_longitude;
    private double min_latitude, max_latitude;

    private boolean loadMapFromWWW; // can we start downloading maps?
    private LinkedList<MockLocation> queue;
    private static int MAX_LOCS = 5;  // max number of last MockLocations to determine direction
    private static int MIN_LOCS = 3;
    private boolean tooBig;  // map is too big to be loaded in one piece
    private boolean isOcourse;

    private Double[] oldDirectionVector;

    public MockGPXUtil(){
        //this.createDirForGPXFiles();
        gpxFileNames = new ArrayList<String>();
        gpxFileName = null;
        this.min_longitude = 500d;
        this.max_longitude = -500d;
        this.min_latitude = 500d;
        this.max_latitude = -500d;
        this.loadMapFromWWW = false;   // can we load map from internet?
        queue = new LinkedList<>();  // max x last MockLocations
        tooBig = true;
        this.isOcourse = false;
        oldDirectionVector = null;
        this.startMockLocation = null;
    }


    public void setloadMapFromWWW(boolean loadMapFromWWW){this.loadMapFromWWW = loadMapFromWWW;}
    //public boolean getloadMapFromWWW(){return this.loadMapFromWWW;}
    //public void setgpxFileName(String name){this.gpxFileName = name;}

    public String getGpxFileName(){
        return gpxFileName;
    }
    public boolean getIsOcourse() {return this.isOcourse; }

    public void resetPositionQueue(){
        while (!this.queue.isEmpty()) {
            this.queue.removeFirst();
        }
    }


    public void updatePositionQueue(MockLocation loc){
        System.out.println("loc in update:"+ loc.getLongitude()+ " :"+loc.getLatitude());

        this.queue.addLast(loc);

        if (this.queue.size() > MAX_LOCS){
            System.out.println("loc in, removing");
            MockLocation l = this.queue.removeFirst();
        }
    }

    public boolean checkQueueDistance(){
        // chekc that we have moved enought to rotate map
        if (this.queue.getFirst() == null){
            return false;
        }
        if (this.queue.getLast() == null){
            return false;
        }
        if (this.queue.getFirst().distanceTo(this.queue.getLast())< MIN_DIST||
                this.queue.getFirst().distanceTo(this.queue.getLast())> MAX_DIST){
            return false;
        }

        return true;
    }

    public Queue<MockLocation> getQueue(){
        return this.queue;
    }

    public void showQueue(){
        System.out.println("QUEUE LOCATIONS");
        for(MockLocation loc: this.queue) {
            System.out.println("lon:"+ loc.getLongitude()+ " lat:"+loc.getLatitude());
        }
    }

    public Double[] getDirectionVector(){
        //https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average
        double WEIGHT = 0.8;

        //Log.d("GPX queue"," "+this.queue.size());
        if (this.queue == null || this.queue.size()< MIN_LOCS) {
            System.out.println("RETURNING NULL  "+this.queue.size());
            Double[] new_r = null;
            return new_r;
        }else{
            MockLocation oldLoc = null;
            MockLocation loc1 = null;
            MockLocation loc2 = null;
            double sumx=0;
            double sumy=0;

            for(MockLocation loc: this.queue) {
                System.out.println("lon:"+ loc.getLongitude()+ " lon:"+loc.getLatitude());

                if (loc1 == null) {
                    loc1 = loc;
                } else {
                    loc2 = loc;
                    //DO IT
                    double dx, dy;
                    dx = loc2.getLongitude() - loc1.getLongitude();
                    dy = loc2.getLatitude() - loc1.getLatitude();
                    //Log.d("DIRECTION"," "+ dx + " " + dy);

                    System.out.println("lon1:"+ loc1.getLongitude()+ " lon2:"+loc2.getLongitude());
                    System.out.println("dlon:"+ dx+ " dlat:"+dy);

                    sumx = WEIGHT * dx + (1 - WEIGHT) * sumx;
                    sumy = WEIGHT * dy + (1 - WEIGHT) * sumy;
                    loc1 = loc2;
                }
            }

            Double[] new_r = new Double[]{sumy, sumx};
            System.out.println("DIRECTION SUM"+ new_r[0]+ " "+new_r[1]);
            Log.i("DIRECTION SUM:"," "+ new_r[0]+ " "+new_r[1]);
            return new_r;
        }
    }

    public char checkDirection(char oldDirection){
        // returns old direction if old and new direction are about the same
        // also returnsold direction if last and first MockLocations in the
        // MockLocation queue are about the same (we are not moving)

        if (queue.size() < MIN_LOCS){
            return oldDirection;
        }

        double minDist = 30; // one must move 30m in 50s
        // otherwise return new direction
        Double[] r = this.getDirectionVector();

        if (queue.getFirst().distanceTo(queue.getLast()) < minDist){
            System.out.println("Returning old");
            return oldDirection;
        }
        double angle1_rad = Math.atan2(r[0], r[1]);
        System.out.println("Angle rad:"+angle1_rad);
        if (oldDirectionVector != null) {
            double angle2_rad = Math.atan2(oldDirectionVector[0], oldDirectionVector[1]);
            if ((Math.abs(angle1_rad - angle2_rad)) < Math.PI / 4.0) {
                // about the same direction
                return this.getNewDirection(oldDirection, Math.PI / 4.5); // this leaves 10deg as gray area between 'N' 'E' 'S' 'W'
            } else { //totally other direction
                return this.getNewDirection(oldDirection, Math.PI / 4.0);
            }
        }else {
            oldDirectionVector=r;
            return this.getNewDirection(oldDirection, Math.PI / 4.0);
        }
    }

    private char getNewDirection(char oldDirection, double mydeg) {
        // mydeg measures how big is the gray area

        Double[] r = this.getDirectionVector();
        //Log.i("GPXDIREc:", " "+r[0]+" "+r[1]);
        if (r==null){
            return oldDirection;
        }
        double angle_rad = Math.atan2(r[0], r[1]);
        System.out.println("RAD"+angle_rad);
        //double mydeg = Math.PI / 36.0;  // gray area between N, W, E, S
        char newDirection = oldDirection;
        if (angle_rad < (-Math.PI / 2 + mydeg) && angle_rad > (-Math.PI / 2 - mydeg)) {
            newDirection = 'S';
        } else if (angle_rad > (Math.PI - mydeg) && angle_rad < (-Math.PI + mydeg)) {
            newDirection = 'W';
        } else if (angle_rad < (Math.PI / 2 + mydeg) && angle_rad > (Math.PI / 2 - mydeg)) {
            newDirection = 'N';
        } else if (angle_rad < mydeg && angle_rad > -mydeg) {
            newDirection = 'E';
        }
        return newDirection;
    }

/*
    private void createDirForGPXFiles(){
        // create a folder to store gpx tracs which can be show on the map
        // create a File object for the parent directory
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), GPX_FOLDER_NAMES[0]);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                //Log.d("App", "failed to create directory");
            }
        }
    }
*/


    public List<MockLocation> getGPXList(){
        for(MockLocation loc: this.gpxList){
            Log.d("GPX:G:",loc.toString());
        }

        return this.gpxList;
    }
    public void setGPXList(List<MockLocation> gpxList){
        this.gpxList = gpxList;
    }

    public void readGPXTrack(String fileName) throws Exception{
        boolean getWholeList = true;
        gpxList = decodeGPX(fileName, getWholeList);
    }

    public boolean isItOrienteeringCourse(){
        // calculate the average distance between first 10 gpx points
        // if the average distance between points is more than 60m
        // we classify the track as an orienteering course
        if (this.gpxList == null){
            //Log.d("GPXutil","NO TRACK for orienteering check");
            System.exit(0);
        }
        int count_max = min(10, this.gpxList.size()-1);
        float dist=0.0f;
        int count=0;
        for (int i=0; i<count_max; i++){
            dist = dist + gpxList.get(i).distanceTo(gpxList.get(i+1));
            count++;
            }
        dist = dist / count;
        //Log.d("GPX: aver dist", dist+" "+count_max);

        if (dist > 60){
            this.isOcourse = true;
            return true;
            //return false;
        }else{
            this.isOcourse = false;
            return false;
        }
    }


    public boolean isTrackTooBig(int nTiles, long memoryinMegs){
        long memoryNeeded = nTiles*256*256*4*8/1048576L;
        if (memoryNeeded > memoryinMegs*0.8){
            Log.d("GPX","CANNOT LOAD THE WHOLE MAP..."+memoryNeeded+" "+memoryinMegs+" "+nTiles);
            this.tooBig = true;
            return true;
        }else{
            Log.d("GPX","LOADING THE WHOLE MAP..."+memoryNeeded+" "+memoryinMegs+ " "+nTiles);
            this.tooBig = false;
            return false;
        }
    }

    public boolean getTooBig(){
        return this.tooBig;
    }

    public void decodeGpxString(String gpxString, boolean getWholeList){
        // parse gpx data from a string
        final String LAT_START = " lat=\"";
        final String LON_START = " lon=\"";
        final String STOP ="\"";
        List<MockLocation> list = new ArrayList<>();
        List<Double> longitudes = new ArrayList<>();
        List<Double> latitudes = new ArrayList<>();

        int fromIndex=0;
        int slen = gpxString.length();
        int count = 0;
        //Log.d("GPX","START GPX READ");
        while (fromIndex < slen){
            //Log.d("GPX","R0 "+fromIndex);
            int ilat_start = gpxString.indexOf(LAT_START, fromIndex)+LAT_START.length();
            int ilat_stop = gpxString.indexOf(STOP, ilat_start);
            int ilon_start = gpxString.indexOf(LON_START, fromIndex)+LON_START.length();
            int ilon_stop = gpxString.indexOf(STOP, ilon_start);
            //Log.d("GPX","R2 "+ilat_start+" "+ilat_stop+" "+ilon_start+" "+ilon_stop);
            if (ilat_start < fromIndex || ilat_stop < fromIndex || ilon_start < fromIndex || ilon_stop < fromIndex){break;};
            if (ilat_start==-1 || ilon_start==-1 || ilat_stop==-1 || ilon_stop==-1) {break;};
            String slat = gpxString.substring(ilat_start, ilat_stop);
            String slon = gpxString.substring(ilon_start, ilon_stop);
            //Log.d("GPX","R3 "+slat+" "+slon);
            Double newLatitude_double = Double.parseDouble(slat);
            Double newLongitude_double = Double.parseDouble(slon);
            String newMockLocationName = String.valueOf(count) + ":" + String.valueOf(count);
            MockLocation newMockLocation = new MockLocation();
            newMockLocation.setLatitude(newLatitude_double);
            newMockLocation.setLongitude(newLongitude_double);

            list.add(newMockLocation);
            longitudes.add(newLongitude_double);
            latitudes.add(newLatitude_double);
            count++;
            fromIndex = ilon_stop + 1;
            //Log.d("GPX from"," "+fromIndex+" "+slen);
        }
        this.min_longitude = Collections.min(longitudes);
        this.max_longitude = Collections.max(longitudes);
        this.min_latitude = Collections.min(latitudes);
        this.max_latitude = Collections.max(latitudes);
        this.loadMapFromWWW = true; // track is ok, we can now do the map
        this.startMockLocation = list.get(0);
        this.setGPXList(list);
    }


    public List<MockLocation> decodeGPX(String fileName, boolean getWholeList){
        // read gpx track to a list of MockLocation objects

        //new File(Environment.getExternalStorageDirectory(), GpxFolderName);
        File file = new File(fileName);
        //File file = new File(Environment.getExternalStorageDirectory()+"/"+
        //        this.GpxFolderName+"/"+fileName);
        List<MockLocation> list = new ArrayList<>();
        List<Double> longitudes = new ArrayList<>();
        List<Double> latitudes = new ArrayList<>();

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            FileInputStream fileInputStream = new FileInputStream(file);
            Document document = documentBuilder.parse(fileInputStream);
            Element elementRoot = document.getDocumentElement();

            NodeList nodelist_trkpt = elementRoot.getElementsByTagName("trkpt");

            for(int i = 0; i < nodelist_trkpt.getLength(); i++){

                Node node = nodelist_trkpt.item(i);
                NamedNodeMap attributes = node.getAttributes();

                String newLatitude = attributes.getNamedItem("lat").getTextContent();
                Double newLatitude_double = Double.parseDouble(newLatitude);

                String newLongitude = attributes.getNamedItem("lon").getTextContent();
                Double newLongitude_double = Double.parseDouble(newLongitude);

                String newMockLocationName = newLatitude + ":" + newLongitude;
                MockLocation newMockLocation = new MockLocation();
                newMockLocation.setLatitude(newLatitude_double);
                newMockLocation.setLongitude(newLongitude_double);

                list.add(newMockLocation);
                longitudes.add(newLongitude_double);
                latitudes.add(newLatitude_double);

                // sometimes we only need the first MockLocation
                if (!getWholeList){break;}

            }
            this.min_longitude = Collections.min(longitudes);
            this.max_longitude = Collections.max(longitudes);
            this.min_latitude = Collections.min(latitudes);
            this.max_latitude = Collections.max(latitudes);
            fileInputStream.close();
        } catch (ParserConfigurationException e) {
            // Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            //
            e.printStackTrace();
        } catch (SAXException e) {
            //
            e.printStackTrace();
        } catch (IOException e) {
            // Auto-generated catch block
            e.printStackTrace();
        }
        this.loadMapFromWWW = true; // track is ok, we can now do the map
        this.startMockLocation = list.get(0);
        return list;
    }

    public MockLocation getStartMockLocation(){
        return this.startMockLocation;
    }

    public void setMaxMinLatLon(List<MockLocation> gpxList){
        List<Double> longitudes = new ArrayList<>();
        List<Double> latitudes = new ArrayList<>();
        for(MockLocation loc:gpxList){
            latitudes.add(loc.getLatitude());
            longitudes.add(loc.getLongitude());
        }
        this.min_longitude = Collections.min(longitudes);
        this.max_longitude = Collections.max(longitudes);
        this.min_latitude = Collections.min(latitudes);
        this.max_latitude = Collections.max(latitudes);
    }

    public Double getMinLongitude(){
        return this.min_longitude;
    }
    public Double getMaxLongitude(){
        return this.max_longitude;
    }
    public Double getMinLatitude(){
        return this.min_latitude;
    }
    public Double getMaxLatitude(){
        return this.max_latitude;
    }


}
