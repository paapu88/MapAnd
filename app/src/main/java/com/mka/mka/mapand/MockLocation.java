package com.mka.mka.mapand;

/**
 * Created by xkaukone on 9.3.2018.
 */

public class MockLocation {
    static final double _eQuatorialEarthRadius = 6378.1370D;
    static final double _d2r = (Math.PI / 180D);

    double latitude;
    double longitude;

    public MockLocation() {
        latitude = 0.0;
        longitude = 0.0;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public float distanceTo(MockLocation loc2) {
        double long2 = loc2.getLongitude();
        double lat2 = loc2.getLatitude();
        double long1 = this.getLongitude();
        double lat1 = this.getLatitude();

        double dlong = (long2 - long1) * _d2r;
        double dlat = (lat2 - lat1) * _d2r;
        double a = Math.pow(Math.sin(dlat / 2D), 2D) + Math.cos(lat1 * _d2r) * Math.cos(lat2 * _d2r)
                * Math.pow(Math.sin(dlong / 2D), 2D);
        double c = 2D * Math.atan2(Math.sqrt(a), Math.sqrt(1D - a));
        double d = _eQuatorialEarthRadius * c;

        return (float) (d*1000D);
    }
}
