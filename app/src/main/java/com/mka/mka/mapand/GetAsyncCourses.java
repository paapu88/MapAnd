package com.mka.mka.mapand;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by mka on 11.2.2018.
 */

public class GetAsyncCourses {
    /*
    Get gpx points from heroku
     */
    class CourseClient extends AsyncTask<String, Void, String> {
        private Exception exception;
        private String TAG="GET GPX:";
        private String courses = "";
        private MainActivity mainActivity;
        public CourseClient(MainActivity main){
            super();
            this.mainActivity = main;
        }
        @Override
        public String doInBackground(String... urls) {
            URL url=null;
            HttpsURLConnection con = null;
            InputStream in = null;
            BufferedReader br = null;
            String lines = "";
            try {
                //Log.d(TAG, "*******************    Open Connection    *****************************");
                try {
                    url = new URL(urls[0]);
                }catch(Exception e){
                }
                //Log.d(TAG, "Received URL:  ");
                try {
                    con = (HttpsURLConnection) url.openConnection();
                }catch(Exception e){

                }
                //Log.d(TAG, "Con Status: " + con);

                try {
                    in = con.getInputStream();
                }catch(Exception e){

                }
                //Log.d(TAG, "GetInputStream:  " + in);

                //Log.d(TAG, "*******************    String Builder     *****************************");
                String line = null;

                try{
                    br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                }catch(Exception e){

                }

                try {
                    line = br.readLine();
                }catch(Exception e){
                    line=null;
                }
                while (line  != null) {
                    //Log.d(TAG, line);
                    lines = lines + line;
                    try {
                        line = br.readLine();
                    }catch(Exception e){
                        line=null;
                    }
                }
                // Closing the stream
                //Log.d(TAG, "*******************  Stream closed, exiting     ******************************");
                br.close();
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
            JSONObject obj=null;
            JSONArray geodata=null;
            try{
                obj = new JSONObject(lines);
            }catch(Exception e)
            {e.printStackTrace();}
            try{
                geodata = obj.getJSONArray("tracks");
            }catch(Exception e)
            {e.printStackTrace();}
            final int n = geodata.length();
            for (int i = 0; i < n; ++i) {
                JSONObject course=null;
                try{course = geodata.getJSONObject(i);}catch(Exception e){e.printStackTrace();}
                //try{Log.d("TR:", course.getString("name"));}catch(Exception e){e.printStackTrace();}
                //try{Log.d("TR:", course.getString("date"));}catch(Exception e){e.printStackTrace();}
                try{courses=courses+course.getString("date")+"_"+course.getString("name")+" ";}catch(Exception e){e.printStackTrace();}
            }
            return courses;
        }
        @Override
        protected void onPostExecute(String result) {
            //Log.d("POST::",result);
            mainActivity.setCourseNames(result);
        }
    }
}
